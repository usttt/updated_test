<?php

namespace App\Imports;

use App\Models\InsuranceCase;
use Maatwebsite\Excel\Concerns\ToModel;

class InsuranceImport implements ToModel
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new InsuranceCase([
            //
        ]);
    }
}
