<?php

namespace App\Http\Controllers;

use App\Imports\InsuranceImport;
use App\Models\Clinics;
use App\Models\Insurance;
use App\Models\InsuranceCase;
use App\Models\InsuranceCaseRelation;
use App\Models\Patients;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;


class InsuranceCaseController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){

            $insurance_case = InsuranceCase::with(['clinic', 'patient', 'insurance'])->orderBy('id', 'ASC');

            return DataTables::eloquent($insurance_case)
                ->addColumn('insurance', function ($insurance_case) {
                    return $insurance_case->insurance->map(function($insurance) {
                        return $insurance->name;
                    })->implode('<br>');
                })
                ->escapeColumns(null)
                ->make(true);
        }

        $url = $this->getUrl();

        return view('insurance_case.index', compact('url'));
    }

    public function create()
    {
        $clinics = Clinics::all();

        if(Auth::user()->role == 3)
        {
            $clinics = Clinics::where('manager_id', Auth::user()->id)->get();
        }

        $url = $this->getUrl();

        return view('insurance_case.create', compact('clinics', 'url'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'clinic_id' => 'required|exists:clinics,id',
            'file' => 'required',
        ]);

        $clinic_id = $request->input('clinic_id');
        $file = Excel::toArray(new InsuranceImport(), request()->file('file'));

        $datas = [];
        $counter = 0;

        /*
         * Обрабатываем файл Excel и добавляем в новый массив
         */

        foreach ($file[1] as $key => $client_data)
        {
            if($client_data[0] && $client_data[1])
            {
                $counter = $counter+1;
                $datas[] = [ 'patient' => $client_data[1], 'phone' => $client_data[3]];
            }

            if(!strpos($client_data[1], ',') && is_string($client_data[1]))
            {

                $datas[$counter-1]['insurance'][] = $client_data[1];
            }

            if(is_int($client_data[1]) )
            {
                $datas[$counter-1]['insurance_case'][] =[
                    'price' => $client_data[5],
                    'date' => Carbon::instance(\PhpOffice\PhpSpreadsheet\Shared\Date::excelToDateTimeObject($client_data[1]))->format('d.m.Y'),
                    'name' => $client_data[4],
                ];
            }

        }

        /*
         * Записываем в базу данных
         */

        try {
            DB::beginTransaction();
            foreach ($datas as $data)
            {
                $patient = Patients::updateOrCreate([
                    'phone' => $data['phone'],
                    'name' => $data['patient']],
                    [
                        'phone' => $data['phone'],
                        'name' => $data['patient'],
                    ]);

                foreach ($data['insurance_case'] as $insurance_case)
                {
                    $case = InsuranceCase::create(
                        [
                            'patient_id' => $patient->id,
                            'name' => $insurance_case['name'],
                            'date' => $insurance_case['date'],
                            'who_created' => Auth::user()->name,
                            'price' => $insurance_case['price'],
                            'status' => 'created',
                            'clinic_id' => $clinic_id
                        ]);

                    foreach ($data['insurance'] as $insurance)
                    {
                        $insurance_create = Insurance::updateOrCreate([
                            'name' => $insurance],
                            [
                                'name' => $insurance,
                            ]);

                        InsuranceCaseRelation::updateOrCreate([
                            'insurance_id' => $insurance_create->id,
                            'insurance_case_id' => $case->id
                        ],
                            [
                                'insurance_id' => $insurance_create->id,
                                'insurance_case_id' => $case->id
                            ]);

                    }
                }
            }

            DB::commit();
        } catch (\Exception $e) {

            DB::rollback();
            return redirect()->back()->withErrors($e->getMessage());
        }

        return back()->with('message', 'Запись успешно добавлен');
    }

    public function destroy($id)
    {
        $clinic = Clinics::find($id)->delete();

        return  Response()->json($clinic);
    }

    private function getUrl()
    {
        $url = '/insurance_case';

        if(Auth::user()->role == 3)
        {
            $url = '/manager/insurance_case';
        }

        return $url;
    }
}
