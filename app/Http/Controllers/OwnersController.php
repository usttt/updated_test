<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class OwnersController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){

            $users = User::where('role', 2);

            return DataTables::eloquent($users)

                ->addColumn('action', function ($user) {

                    $btns = '<a href="javascript:void(0)"  onclick="deleteData('.$user->id.')" class="btn btn-danger"><i class="fas fa-trash-o"></i> Удалить</a>';
                    $btns .= ' <a href="' . url('/owners/'.$user->id.'/edit'). '"  class="btn btn-warning"><i class="fas fa-edit"></i>Изменить</a>';
                    return $btns;
                })
                ->escapeColumns(null)
                ->make(true);
        }

        return view('owners.index');
    }

    public function create()
    {
        return view('owners.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|string|digits:8|confirmed',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $password = $request->input('password');

        $user = new User();

        $user->name = $name;

        $user->phone = $phone;
        $user->role = 2;
        $user->password = Hash::make($password);
        $user->save();

        return back()->with('message', 'Запись успешно добавлен');
    }

    public function edit($id)
    {

        $data = User::find($id);

        return view('owners.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$id.'id',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $password = $request->input('password');

        $user = User::find($id);

        if(!is_null($password))
        {
            $this->validate($request, [
                'password' => 'required|string|digits:8|confirmed',
            ]);

            $user->password = Hash::make($password);
        }

        $user->name = $name;
        $user->phone = $phone;

        $user->save();

        return back()->with('message', 'Запись успешно изменен');
    }

    public function destroy($id)
    {
        $user = User::find($id)->delete();

        return  Response()->json($user);
    }

}
