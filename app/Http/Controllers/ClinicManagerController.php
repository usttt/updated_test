<?php

namespace App\Http\Controllers;

use App\Models\Clinics;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class ClinicManagerController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){

            $users = User::where('role', 3)->with('clinics');

            return DataTables::eloquent($users)

                ->addColumn('action', function ($user) {

                    $btns = '<a href="javascript:void(0)"  onclick="deleteData('.$user->id.')" class="btn btn-danger"><i class="fas fa-trash-o"></i> Удалить</a>';
                    $btns .= ' <a href="' . url('/clinic_manager/'.$user->id.'/edit'). '"  class="btn btn-warning"><i class="fas fa-edit"></i>Изменить</a>';
                    return $btns;
                })
                ->addColumn('clinics', function ($user) {
                    return $user->clinics->map(function($clinic) {
                        return $clinic->name;
                    })->implode('<br>');
                })
                ->escapeColumns(null)
                ->make(true);
        }

        return view('clinic_manager.index');
    }

    public function create()
    {
        $clinics = Clinics::all();

        return view('clinic_manager.create', compact('clinics'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users,phone',
            'password' => 'required|string|digits:8|confirmed',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $clinics = $request->input('clinic_id');

        $user = new User();

        $user->name = $name;

        $user->phone = $phone;
        $user->role = 3;
        $user->password = Hash::make($password);
        $user->save();

        if(isset($clinics))
        {
            foreach ($clinics as $clinic)
            {
                $clinic_create = Clinics::find($clinic);
                $clinic_create->manager_id = $user->id;

                $clinic_create->save();
            }
        }

        return back()->with('message', 'Запись успешно добавлен');
    }

    public function edit($id)
    {
        $data = User::find($id);

        $clinics = Clinics::all();

        return view('clinic_manager.edit', compact('data', 'clinics'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'phone' => 'required|unique:users,phone,'.$id.'id',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $password = $request->input('password');
        $clinics = $request->input('clinic_id');

        $user = User::find($id);

        if(!is_null($password))
        {
            $this->validate($request, [
                'password' => 'required|string|digits:8|confirmed',
            ]);

            $user->password = Hash::make($password);
        }

        $user->name = $name;
        $user->phone = $phone;

        $user->save();

        if(isset($clinics))
        {
            $has_data = $user->clinics->whereNotIn('id', $clinics);

            if($has_data->count())
            {
                foreach ($has_data as $data)
                {
                    $data->manager_id = null;
                    $data->save();
                }
            }

            foreach ($clinics as $clinic)
            {
                $clinic_update = Clinics::find($clinic);
                $clinic_update->manager_id = $user->id;

                $clinic_update->save();
            }
        }else{

            if($user->clinics->count())
            {
                foreach ($user->clinics as $data)
                {
                    $data->manager_id = null;
                    $data->save();
                }
            }
        }

        return back()->with('message', 'Запись успешно изменен');
    }

    public function destroy($id)
    {
        $user = User::find($id)->delete();

        return  Response()->json($user);
    }
}
