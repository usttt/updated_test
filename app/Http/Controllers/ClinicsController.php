<?php

namespace App\Http\Controllers;

use App\Models\Clinics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;


class ClinicsController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()){
            if(Auth::user()->role == 3)
            {
                $clinics = Clinics::where('manager_id', Auth::user()->id);

                return DataTables::eloquent($clinics)

                    ->addColumn('action', function ($clinic) {

                        return '';
                    })
                    ->escapeColumns(null)
                    ->make(true);
            }
            else{
                $clinics = Clinics::orderBy('id', 'ASC');

                return DataTables::eloquent($clinics)

                    ->addColumn('action', function ($clinic) {

                        $btns = '<a href="javascript:void(0)"  onclick="deleteData('.$clinic->id.')" class="btn btn-danger"><i class="fas fa-trash-o"></i> Удалить</a>';
                        $btns .= ' <a href="' . url('/clinics/'.$clinic->id.'/edit'). '"  class="btn btn-warning"><i class="fas fa-edit"></i>Изменить</a>';
                        return $btns;
                    })
                    ->escapeColumns(null)
                    ->make(true);
            }
        }

        $url = '/clinics';

        if(Auth::user()->role == 3)
        {
            $url = '/manager/clinics';
        }

        return view('clinics.index', compact('url'));
    }

    public function create()
    {
        return view('clinics.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|unique:clinics,phone',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');

        $clinic = new Clinics();

        $clinic->name = $name;
        $clinic->phone = $phone;
        $clinic->address = $address;

        $clinic->save();

        return back()->with('message', 'Запись успешно добавлен');
    }

    public function edit($id)
    {
        $data = Clinics::find($id);

        return view('clinics.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|unique:clinics,phone,'.$id.'id',
        ]);

        $name = $request->input('name');
        $phone = $request->input('phone');
        $address = $request->input('address');

        $clinic = Clinics::find($id);

        $clinic->name = $name;
        $clinic->phone = $phone;
        $clinic->address = $address;

        $clinic->save();

        return back()->with('message', 'Запись успешно изменен');
    }

    public function destroy($id)
    {
        $clinic = Clinics::find($id)->delete();

        return  Response()->json($clinic);
    }
}
