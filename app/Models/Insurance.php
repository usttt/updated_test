<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table = 'insurance';

    protected $fillable = ['name'];

    public function insurance_case()
    {
        return $this->belongsToMany(InsuranceCase::class, 'i_c_relation',  'insurance_case_id','insurance_id');
    }
}
