<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InsuranceCase extends Model
{
    protected $table = 'insurance_case';

    protected $fillable = ['name', 'price', 'date', 'status',
        'who_created', 'patient_id', 'clinic_id'];

    public function clinic()
    {
        return $this->belongsTo(Clinics::class,'clinic_id', 'id');
    }

    public function insurance()
    {
        return $this->belongsToMany(Insurance::class, 'i_c_relation', 'insurance_case_id', 'insurance_id' );
    }

    public function patient()
    {
        return $this->belongsTo(Patients::class,'patient_id', 'id');
    }
}
