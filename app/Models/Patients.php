<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $table = 'patients';

    protected $fillable = ['name', 'phone'];

}
