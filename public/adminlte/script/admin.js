function deleteData(id){
    const deleteUrl = window.location.href + '/' + id;
    Swal.fire({
        title: 'Вы точно хотите удалить?',
        text: "Данные будут удалены!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да',
        cancelButtonText: 'Нет'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                type: 'DELETE',
                dataType: 'json',
                data:({"_token": $('meta[name="csrf-token"]').attr('content')}),
                url: deleteUrl,
            }).done(function(success){
                if(success){
                    $('#datatable').DataTable().ajax.reload();
                    Swal.fire(
                        'Удален!',
                        'Данные успешно удалены.',
                        'success'
                    )
                }
            });
        }
    })
}


function deleteAllData(){
    //const deleteUrl = window.location.href + '/all';
    Swal.fire({
        title: 'Вы точно хотите все удалить?',
        text: "Все данные будут удалены!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да',
        cancelButtonText: 'Нет'
    }).then((result) => {
        if (result.value) {
        confirmAllData();
        /*$('#export')[0].click(function () {
            
        });*/
        /*$.ajax({
            type: 'DELETE',
            dataType: 'json',
            data:({"_token": $('meta[name="csrf-token"]').attr('content')}),
            url: deleteUrl,
        }).done(function(success){
            if(success){
                $('#datatable').DataTable().ajax.reload();
                Swal.fire(
                    'Удален!',
                    'Все данные успешно удалены.',
                    'success'
                )
            }
        });*/
    }
})
}

function confirmAllData(){
    const deleteUrl = window.location.href + '/all';
    Swal.fire({
        title: 'Подтвердите что хотите все удалить?',
        text: "Внимание!!! Все данные будут удалены!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да',
        cancelButtonText: 'Нет'
    }).then((result) => {
        if (result.value) {
    
        /*$('#export')[0].click(function () {
            
        });*/
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            data:({"_token": $('meta[name="csrf-token"]').attr('content')}),
            url: deleteUrl,
        }).done(function(success){
            if(success){
                $('#datatable').DataTable().ajax.reload();
                Swal.fire(
                    'Удален!',
                    'Все данные успешно удалены.',
                    'success'
                )
            }
        });
    }
})
}

function banData(id){
    const deleteUrl = window.location.href + '/' + id;
    Swal.fire({
        title: 'Вы точно хотите Заблокировать?',
        text: "Данный пользователь будет заблокирован!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да',
        cancelButtonText: 'Нет'
    }).then((result) => {
        if (result.value) {
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            data:({"_token": $('meta[name="csrf-token"]').attr('content')}),
            url: deleteUrl,
        }).done(function(success){
            if(success){
                $('#datatable').DataTable().ajax.reload();
                Swal.fire(
                    'Забанен!',
                    'Данный пользователь заблокирован.',
                    'success'
                )
            }
        });
    }
})
}


function unLockData(id){
    const deleteUrl = window.location.href + '/' + id;
    Swal.fire({
        title: 'Вы точно хотите Разблокировать?',
        text: "Данный пользователь Разблокируется!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Да',
        cancelButtonText: 'Нет'
    }).then((result) => {
        if (result.value) {
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            data:({"_token": $('meta[name="csrf-token"]').attr('content')}),
            url: deleteUrl,
        }).done(function(success){
            if(success){
                $('#datatable').DataTable().ajax.reload();
                Swal.fire(
                    'Разблокирован!',
                    'Данный пользователь разблокирован.',
                    'success'
                )
            }
        });
    }
})
}

function initDatatable (url,...columns) {
    console.log(url)
    var table = $('#datatable').DataTable({
        ajax: url,
        searching: true,
        ordering: true,
        processing: true,
        serverSide: true,
        autoWidth: true,
        lengthChange: true,
        lengthMenu: [ [10, 25, 50, -1], [10, 25, 50, "All"] ],
        columns: columns,
        dom: '<"toolbar"frtip><"datatable-scroll"t><"datatable-footer"ip>',
        language: {
            search: '<span>Search:</span> _INPUT_',
            lengthMenu: '<span>Show:</span> _MENU_',
            paginate: { 'first': 'First', 'last': 'Last', 'next': '→', 'previous': '←' }
        }
    });
}
