<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(
    [
        'middleware' => ['web', 'auth']
    ],
    function(){

        Route::get('/', function () {
            return view('welcome');
        });
    });



Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(
    [
        'middleware' => ['web', 'auth', 'is_admin']
    ],
    function(){

        Route::resource('/owners', 'OwnersController');

    });

Route::group(
    [
        'middleware' => ['web', 'auth', 'is_owner']
    ],
    function(){

        Route::resource('/clinics', 'ClinicsController');
        Route::resource('/clinic_manager', 'ClinicManagerController');
        Route::resource('/insurance_case', 'InsuranceCaseController');
    });

Route::group(
    [
        'middleware' => ['web', 'auth', 'is_manager']
    ],
    function(){

        Route::get('manager/clinics', 'ClinicsController@index');
        Route::resource('manager/insurance_case', 'InsuranceCaseController');
    });

