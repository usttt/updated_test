<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class CreateUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::firstOrCreate(['phone' => '900900900'],[
            'name'		=> 'Admin',
            'phone'		=> '900900900',
            'role'		=> 1,
            'password'	=> bcrypt('admin'),
        ]);

        User::firstOrCreate(['phone' => '800800800'],[
            'name'		=> 'Owner',
            'phone'		=> '800800800',
            'role'		=> 2,
            'password'	=> bcrypt('owner'),
        ]);

        User::firstOrCreate(['phone' => '700700700'],[
            'name'		=> 'Manager',
            'phone'		=> '700700700',
            'role'		=> 3,
            'password'	=> bcrypt('manager'),
        ]);
    }
}
