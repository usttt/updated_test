<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateICRelationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('i_c_relation', function (Blueprint $table) {
            $table->bigInteger('insurance_id')->unsigned()->index()->nullable();
            $table->foreign('insurance_id')->references('id')->on('insurance')
                ->onDelete('set null')->onUpdate('cascade');
            $table->bigInteger('insurance_case_id')->unsigned()->index()->nullable();
            $table->foreign('insurance_case_id')->references('id')->on('insurance_case')
                ->onDelete('set null')->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('i_c_relation');
    }
}
