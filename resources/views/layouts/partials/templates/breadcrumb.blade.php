@foreach($breadcrumbs as $bread)
    @if($loop->last)
        <li class="breadcrumb-item active">{{$bread}}</li>
    @else
        <li class="breadcrumb-item">{{$bread}}</li>
    @endif
@endforeach
