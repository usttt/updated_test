<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">

        <span class="brand-text font-weight-light">{{Auth::check() ? Auth::user()->name : ''}}</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->


        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                @if(Auth::user()->role == 1)
                <li class="nav-item">

                    <a href="{{url('owners')}}" class="nav-link
                         {{request()->is('owners*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Владельцы сети
                        </p>
                    </a>
                </li>
                @endif

                @if(Auth::user()->role == 2)
                <li class="nav-item">

                    <a href="{{url('clinics')}}" class="nav-link
                         {{request()->is('clinics*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-building"></i>
                        <p>
                            Клиники
                        </p>
                    </a>
                </li>

                <li class="nav-item">

                    <a href="{{url('clinic_manager')}}" class="nav-link
                     {{request()->is('clinic_manager*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Управляющие клиники
                        </p>
                    </a>
                </li>

                <li class="nav-item">

                    <a href="{{url('insurance_case')}}" class="nav-link
                 {{request()->is('insurance_case*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-list"></i>
                        <p>
                            Страховые случаи
                        </p>
                    </a>
                </li>
                @endif

                @if(Auth::user()->role == 3)
                    <li class="nav-item">

                        <a href="{{url('manager/clinics')}}" class="nav-link
                         {{request()->is('manager/clinics*') ? 'active' : ''}}">
                            <i class="nav-icon fas fa-building"></i>
                            <p>
                                Клиники
                            </p>
                        </a>
                    </li>

                    <li class="nav-item">

                        <a href="{{url('manager/insurance_case')}}" class="nav-link
                            {{request()->is('manager/insurance_case*') ? 'active' : ''}}">
                            <i class="nav-icon fas fa-list"></i>
                            <p>
                                Страховые случаи
                            </p>
                        </a>
                    </li>
                @endif
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
