<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Администратор | Войти</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{asset('adminlet/plugins/fontawesome-free/css/all.min.css')}}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="{{asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{asset('adminlte/dist/css/adminlte.min.css')}}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="card">
    @if($errors->get('login') || $errors->get('incorrect'))
        @php
            $flash_error = '';
        @endphp

        @if($errors->get('login'))
            @php
                $flash_error = $errors->get('login')[0];
            @endphp
        @elseif($errors->get('incorrect'))
            @php
                $flash_error = $errors->get('incorrect')[0];
            @endphp
        @endif


        <!-- /.box-header -->
            <div class="alert alert-danger">
                {{ $flash_error }}
            </div>
            <!-- /.box-body -->
    </div>
    @endif
    <div class="card-body login-card-body">
        <p class="login-box-msg">Администратор</p>
        <form action="{{route('admin.login.auth')}}" method="post">
            @csrf
            <div class="input-group mb-3">
                <input type="text" name="login" class="form-control" placeholder="Логин">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" name="password" class="form-control" placeholder="Пароль">
                <div class="input-group-append">
                    <div class="input-group-text">
                        <span class="fas fa-lock"></span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    {!! NoCaptcha::display(); !!}
                </div>
                @if ($errors->has('g-recaptcha-response'))
                    <span class="help-block">
        <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
    </span>
            @endif
            <!-- /.col -->
                <div class="col-md-4 offset-8 mt-2">
                    <button type="submit" class="btn btn-primary btn-block">Войти</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>

</div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('adminlte/dist/js/adminlte.min.js')}}"></script>
{!! NoCaptcha::renderJs() !!}
</body>
</html>
