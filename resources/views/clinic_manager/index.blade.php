@extends('layouts.partials.layout')
@section('header')
    Управляющие клиники
@endsection

@section('breadcrumb')
    @include('layouts.partials.templates.breadcrumb', ['breadcrumbs' => ['Управляющие клиники']])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Управляющие клиники</h3>


            <a href="{{ url('/clinic_manager/create') }}"  class="btn btn-success float-right ml-4">
                <b><i class="fa fa-plus"></i> </b> Добавить Запись
            </a>

        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Имя</th>
                        <th>Телефон</th>
                        <th>Клиники</th>
                        <th>Действие</th>
                    </tr>
                </thead>

            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@section('script')
    <script>
        initDatatable("{!!url('/clinic_manager') !!}" ,
            { data: 'id', name: 'id'},
            { data: "name", name: 'name'},
            { data: "phone", name: 'phone'},
            { data: "clinics", name: 'clinics'},
            { data: 'action', name: 'action', orderable: false, searchable: false, width:400})
    </script>
@endsection
