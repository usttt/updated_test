@extends('layouts.partials.layout')
@section('header')
    Клиники
@endsection

@section('breadcrumb')
    @include('layouts.partials.templates.breadcrumb', ['breadcrumbs' => ['Клиники']])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            @if (count($errors))
                <div class="form-group">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if(\Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p><i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Клиники</h3>
                </div>
                <form role="form" action="{{url('/clinics')}}" method="post"
                      enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="name">Имя<span class="text-danger">*</span></label>
                            <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}"
                                   required>
                        </div>

                        <div class="form-group">
                            <label for="address">Адрес<span class="text-danger">*</span></label>
                            <input type="text" name="address" id="address" class="form-control" value="{{old('address')}}">
                        </div>

                        <div class="form-group">
                            <label for="phone">Телефон<span class="text-danger">*</span></label>
                            <input type="text" name="phone" id="phone" class="form-control" value="{{old('phone')}}">
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
