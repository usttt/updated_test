@extends('layouts.partials.layout')
@section('header')
    Страховые случаи
@endsection

@section('breadcrumb')
    @include('layouts.partials.templates.breadcrumb', ['breadcrumbs' => ['Страховые случаи']])
@endsection

@section('content')
    <div class="row">
        <div class="col-md-8">
            @if (count($errors))
                <div class="form-group">
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            @endif
            @if(\Session::has('message'))
                <div class="alert alert-success" role="alert">
                    <p><i class="fa fa-check-circle-o"> </i> {{Session::get('message')}} </p>
                </div>
            @endif
            <div class="card card-primary">
                <div class="card-header">
                    <h3 class="card-title">Страховые случаи</h3>
                </div>
                <form role="form" action="{{url($url)}}" method="post"
                      enctype="multipart/form-data">
                    @csrf

                    <div class="card-body">
                        <div class="form-group">
                            <label for="file">Клиника<span class="text-danger">*</span></label>
                            <select name="clinic_id" id="" class="form-control">
                                @foreach($clinics as $clinic)
                                <option value="{{$clinic->id}}">{{$clinic->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="form-group">
                            <label for="file">Файл Excel<span class="text-danger">*</span></label>
                            <input type="file" name="file" id="file" class="form-control"
                                   required>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary float-right">Добавить</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
