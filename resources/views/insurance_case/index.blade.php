@extends('layouts.partials.layout')
@section('header')
    Страховые случаи
@endsection

@section('breadcrumb')
    @include('layouts.partials.templates.breadcrumb', ['breadcrumbs' => ['Страховые случаи']])
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Страховые случаи</h3>


            <a href="{{ url($url.'/create') }}"  class="btn btn-success float-right ml-4">
                <b><i class="fa fa-plus"></i> </b> Добавить Запись
            </a>

        </div>
        <!-- /.card-header -->
        <div class="card-body table-responsive">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>№</th>
                        <th>Пациент</th>
                        <th>Телефон</th>
                        <th>Услуга</th>
                        <th>Цена</th>
                        <th>Дата</th>
                        <th>Статус</th>
                        <th>Создал</th>
                        <th>Страховая</th>
                        <th>Клиника</th>
                    </tr>
                </thead>

            </table>
        </div>
        <!-- /.card-body -->
    </div>
@endsection

@section('script')
    <script>
        initDatatable("{!!url($url) !!}" ,
            { data: 'id', name: 'id'},
            { data: "patient.name", name: 'patient'},
            { data: "patient.phone", name: 'patient'},
            { data: "name", name: 'name'},
            { data: "price", name: 'price'},
            { data: "date", name: 'date'},
            { data: "status", name: 'status'},
            { data: "who_created", name: 'who_created'},
            { data: "insurance", name: 'insurance'},
            { data: "clinic.name", name: 'clinic'})
    </script>
@endsection
